﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoProject.Models
{
    [MetadataType(typeof(NewsMetadata))]
    public partial class News
    {
    }

    public class NewsMetadata
    {
        [DisplayName("標題")]
        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [DisplayName("內容")]
        [Required]
        [StringLength(1000)]
        public string Content { get; set; }

        [DisplayName("建立時間")]
        public System.DateTime CreateTime { get; set; }

        [DisplayName("是否隱藏")]
        public bool isHidden { get; set; }
    } 

}